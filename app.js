const Discord = require("discord.js");
const ddiff = require("return-deep-diff");
const client = new Discord.Client();
const token = require("./settings.json").token; // ./ = current directory

const lolChampData = require("./LeagueOfLegends/lolChamp.json").data;
const lolItemData = require("./LeagueOfLegends/lolItem.json").data;



// Komut İstemi'nde botu 
// "node app.js" komutuyla çalıştırdıktan sonra 
// bize döndüreceği yanıt fonksiyonu.

// nodemon


var chs = [];
var prefix = "_";

// Ready Function
client.on("ready", () => {
	console.log("Merhaba!");

//Sunucudaki tüm metin kanallarını chs adlı array'de topluyoruz.
	var counter = 1;
	client.channels.forEach((ch) => {
			if(ch.type === "text"){
				chs[counter] = ch;
				counter++;
			}
		})

});


client.login(token);


// Sunucuya katılma & Sunucudan ayrılma kaydı.
client.on("guildMemberAdd", member => {
	let guild = member.guild;
	guild.channels.get('553264988601450546').send(`${member.user.username} sunucuya katıldı!, Tarih: ${new Date()}`);
});

client.on("guildMemberRemove", member => {
	let guild = member.guild;
	guild.channels.get('553264988601450546').send(`${member.user.username} artık aramızda yok! :(, Tarih: ${new Date()}`);
});

client.on("guildMemberSpeaking", (member, speaking) => {
	let guild = member.guild;
	if(member.speaking){
		guild.channels.get('553264988601450546').send(`${member.user.username} konuşuyor!`);
	}
});

// ____________________________________________________________________________________________________________________________

client.on("guildMemberUpdate", (oMember, nMember) => {
	console.log(ddiff(oMember, nMember));
});

client.on("guildUpdate", (oGuild, nGuild) => {
	console.log(ddiff(oGuild, nGuild));
});

// ____________________________________________________________________________________________________________________________

client.on("channelCreate", channel =>{
	channel.guild.channels.get('553264988601450546').send(`${channel.name} (${channel.type}) kanalı ${channel.id} id'si ile oluşturuldu!`);

	if(channel.type === "text"){
		channel.send("Selamuğaleyküm");
	}
});

client.on("channelDelete", channel =>{
	channel.guild.channels.get('553264988601450546').send(`${channel.id} id'li ${channel.name} (${channel.type}) kanalı silindi!`);
});

client.on("channelUpdate", (oChannel, nChannel) => {
	console.log(ddiff(oChannel, nChannel));
});

// ____________________________________________________________________________________________________________________________

client.on("message", message =>{

	args = message.content.split(' ').slice(1);
	var argresult = args.join(' ');
	

	if(message.content.startsWith(prefix + "kullanıcılarılistele")){
		message.guild.members.forEach((m) => {
				message.channel.send(m.displayName + " \t - \t id: " + m.id);
		})
	}

	if(message.content.startsWith(prefix + "kanallarılistele")){


		var counter = 1;

		chs.forEach((ch) => {

				message.channel.send(counter + "\t - \t " + ch.name + " \t - \t id: " + ch.id);
				counter++;			
		})
	}

	if(message.content.startsWith(prefix + "kanalataşı")){

		uclu_komut = message.content.split(' ', 3);

		var msg = message.channel.fetchMessage(uclu_komut[2]);

		let embedMessage = new Discord.RichEmbed()
			.setTitle("Yanlış Kanal Kullanımı!")
			.setAuthor(message.author.username, message.author.avatarURL)
			.addField("Taşınma İşlemi", `${message.channel.name}` + " kanalından " + client.channels.get(chs[uclu_komut[1]].id).name + " kanalına.");

		msg.then(message => {

		  	embedMessage.setDescription("*Mesaj içeriği:*  " + message.content);

		  	message.attachments.forEach((attachment) => {

		  		//message.channel.send("Kullanıcı tarafından dosya yüklendi!");
		  		client.channels.get(chs[uclu_komut[1]].id).send(message.content, {files: [attachment.url]});
		  	})




		  	message.embeds.forEach((embed) => {

		  		// normal, 
		  		if(embed.type == "link"){
		  			embedMessage.setDescription("Yukarıdaki link taşındı!");
		  			client.channels.get(chs[uclu_komut[1]].id).send(message.content);
		  		}

		  		// youtube, twitch, instagram video,    
		  		if(embed.type == "video"){
		  			embedMessage.setDescription("Yukarıdaki link taşındı!");
		  			client.channels.get(chs[uclu_komut[1]].id).send(message.content);
		  		}

		  		// twitter, 
		  		if(embed.type == "rich"){
		  			embedMessage.setDescription("Yukarıdaki link taşındı!");
		  			client.channels.get(chs[uclu_komut[1]].id).send(message.content);
		  		}

		  		// instagram photo, 
		  		if(embed.type == "image"){

		  			embedMessage.setDescription("Yukarıdaki link taşındı!");
		  			client.channels.get(chs[uclu_komut[1]].id).send(message.content);

		  		//	embedMessage.setDescription("*Görsel Linki:*  " + message.content);
		  		//	embedMessage.setImage(embed.thumbnail.url);
		  		}
		  		message.channel.send(embed.type);
		  	});

		  	
		  	client.channels.get(chs[uclu_komut[1]].id).send(embedMessage);


		  	//message.delete(1000);

		})
	}
})



client.on("message", message =>{

	args = message.content.split(' ').slice(1);
	var argresult = args.join(' ');

		

	if(message.content.startsWith(prefix + "şampiyon")){


		var champ = lolChampData[argresult]
		var champName = champ.name;
		var url = "http://ddragon.leagueoflegends.com/cdn/9.5.1/img/champion/" + champName + ".png"
	

		let champion = new Discord.RichEmbed()
		.setTitle(champ.name + " | " + champ.title)
		.setThumbnail(url)
		.setDescription("Can : " + champ.stats.hp + " (+ " + champ.stats.hpperlevel + ")" +
			"\nMana : " + champ.stats.mp + " (+ " + champ.stats.mpperlevel + ")" +
			"\nHareket Hızı : " + champ.stats.movespeed + 
			"\nZırh : " + champ.stats.armor + " (+ " + champ.stats.armorperlevel + ")" + 
			"\nBüyü Direnci : " + champ.stats.spellblock + " (+ " + champ.stats.spellblockperlevel + ")" +	
			"\nAttack Range : " + champ.stats.attackrange + 
			"\nCan Yenilenmesi : " + champ.stats.hpregen + " (+ " + champ.stats.hpregenperlevel + ")" +
			"\nMana Yenilenmesi : " + champ.stats.mpregen + " (+ " + champ.stats.mpregenperlevel + ")" +
			"\nKritik : " + champ.stats.crit + " (+ " + champ.stats.critperlevel + ")" +
			"\nSaldırı Hasarı : " + champ.stats.attackdamage + " (+ " + champ.stats.attackdamageperlevel + ")" +
			"\nSaldırı Hızı : " + champ.stats.attackspeed + " (+ " + champ.stats.attackspeedperlevel + ")")
		.setFooter(champ.tags);
		
		message.channel.send(champion);
		
		}

		

		if(message.content.startsWith(prefix + "itemara")){
			for(var item in lolItemData){
				if(lolItemData[item].name.includes(argresult)){
					message.channel.send(lolItemData[item].name + " | " + item);
				}else continue;
			}
			message.channel.send("'_itemgetir item_id' şeklinde ilgili iteme ulaşabilirsiniz.");
		}



		if(message.content.startsWith(prefix + "itemgetir")){

			
			var item = lolItemData[argresult];

			if(item == null){

				message.channel.send("```css\n[BULUNAMADI]\n```");
				return;
			}

			var url = "http://ddragon.leagueoflegends.com/cdn/9.5.1/img/item/";


			function DescriptionReplacement(itemDesc){

			var itemDescription = itemDesc.replace(/<stats>/g, '```');
			itemDescription = itemDescription.replace(/<\/stats>/g, '```');

			itemDescription = itemDescription.replace(/<br>/g, '\n');
			
			itemDescription = itemDescription.replace(/<unique>/g, '**');
			itemDescription = itemDescription.replace(/<\/unique>/g, '**');

			itemDescription = itemDescription.replace(/<mana>/g, '');
			itemDescription = itemDescription.replace(/<\/mana>/g, '');

			itemDescription = itemDescription.replace(/<groupLimit>/g, '');
			itemDescription = itemDescription.replace(/<\/groupLimit>/g, '');

			itemDescription = itemDescription.replace(/<active>/g, '**');
			itemDescription = itemDescription.replace(/<\/active>/g, '**');

			itemDescription = itemDescription.replace(/<passive>/g, '**');
			itemDescription = itemDescription.replace(/<\/passive>/g, '**');

			itemDescription = itemDescription.replace(/<rules>/g, '');
			itemDescription = itemDescription.replace(/<\/rules>/g, '');

			return itemDescription

			}
		


		let selectedItem = new Discord.RichEmbed()
		.setTitle(item.name)
		.setThumbnail(url + item.image.full)
		.setDescription(DescriptionReplacement(item.description))
		.setFooter(item.gold.base + " Altın");

		message.channel.send(selectedItem);



		if(message.content.startsWith(prefix + "itemgetiralt")){

			var item = lolItemData[argresult];

			if(item == null){

				message.channel.send("```css\n[BULUNAMADI]\n```");
				return;
			}


			let fromItem = new Discord.RichEmbed();

			if(item.from != null){
				fromItem.setTitle("Alt İtemler");
				message.channel.send(fromItem);
				fromItem = new Discord.RichEmbed();
			}
			
			
			
			FromItems(item);


			function FromItems(currentItem){
			

				if(currentItem.from != null){	

					for(var i = 0; i < currentItem.from.length; i++){	
							
						var itemDT = currentItem.from[i];

						var itemText_1 = itemDT + " | " + lolItemData[itemDT].name;
						var itemText_2 = lolItemData[itemDT].gold.total + "g ( " + lolItemData[itemDT].gold.base + "g )";
							
						fromItem.addField(itemText_1 + " | -> | Toplam: "+ itemText_2, lolItemData[itemDT].plaintext);
					

						if(lolItemData[itemDT].from != null){
							fromItem.setThumbnail(url + lolItemData[itemDT].image.full);
							fromItem.addBlankField(); // En alt itemler kısmı.
							fromItem.setDescription(DescriptionReplacement(lolItemData[itemDT].description));
							FromItems(lolItemData[itemDT]);

						}		
							//currentItem.from[i]: from'daki numaraları veriyor. 				
					};
					message.channel.send(fromItem);
					fromItem = new Discord.RichEmbed();
				}

			}
		}	
	}
		
});



// Message Event
client.on("message", message => {


	args = message.content.split(' ').slice(1);
	var argresult = args.join(' ');




	if(!message.content.startsWith(prefix)) return;

	

	if(message.content === prefix + "ping"){
		message.channel.send(`Pong: \`${Date.now() - message.createdTimestamp} ms \``);	
	} else

	if(message.content.startsWith(prefix + "kb")){

		message.author.send(":))");

	}
	// .... Oynuyor, şeklinde alta yazı yazmak için
	if(message.content.startsWith(prefix + "etkinlikbelirle")){
		client.user.setActivity(argresult);
	} else

	if(message.content.startsWith(prefix + "setstatus")){
		client.user.setStatus(argresult);
	} else

	if(message.content.startsWith(prefix + "setusername")){
		client.user.setUsername(argresult)
  		.then(user => console.log(`My new username is ${user.username}`))
  		.catch(console.error);
	} else

// Voice Channel (Requires Chocolately -to install ffmpeg- and FFMPEG)
	if(message.content.startsWith(prefix + "kardosesligel")){
		let voiceChan = message.member.voiceChannel;
		if(!voiceChan || voiceChan.type !== "voice"){
			message.channel.send("Abi, bi ses kanalına girersen eşlik edeyim?");
		}else if(message.guild.voiceConnection){
			message.channel.send("Zaten bir ses kanalındayım!");
		}else{
			message.channel.send("Katılıyorum...");
			voiceChan.join();
			message.channel.send("Katıldım!");
		}	
	} else

	if(message.content.startsWith(prefix + "kardosesligit")){
		let voiceChan = message.member.voiceChannel;
		if(!voiceChan){
			message.channel.send("Abi, ses kanalında değilsin!");
		} else {
			message.channel.send("Ayrılıyorum... :cry:");
			voiceChan.leave();
		}
	}



	console.log("Prefix'li mesaj geldi abi ama, bi bak istersen");


		/* Notlar */

		// client.channels.get('CHANNEL_ID').sendMessage("YOUR_MESSAGE_TO_SEND") --> Belli bir kanala mesaj yollama.

		// if(message.content.startsWith === "ping") --> Düz ingiliççe işte.
		// if(message.author === Client.user) return; --> Mesajı atan kişi bot ise... 
		// if(message.author.bot) --> Bu da aynı kapı.

		// message.channel.sendMessage("pong");
		// Çıktı: pong

		// message.reply("pong");
		// Çıktı: @mention, pong
});









